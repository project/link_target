# Link target

This module allows you to add a target to link fields.

You can configure the link target per link if you select the
appropriate field widget in the field settings.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/link_target).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/link_target).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Configure the targets that should be available to the editor in the widget
settings in the Form display tab for your content type or bundle.


## Maintainers

- Martin Anderson-Clutz - [mandclu](https://www.drupal.org/u/mandclu)
- Jacob Francke - [jacobfrancke@gmail.com](https://www.drupal.org/u/jacobfranckegmailcom)
- Meaghan Turner - [mturner20](https://www.drupal.org/u/mturner20)

**This project has been sponsored by:**
- [Nedbase](https://www.drupal.org/nedbase)
- [Northern Commerce](https://www.drupal.org/northern-commerce)